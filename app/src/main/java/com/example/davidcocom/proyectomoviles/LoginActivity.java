package com.example.davidcocom.proyectomoviles;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.dd.morphingbutton.MorphingButton;
import com.example.davidcocom.proyectomoviles.db.CodesDataSource;
import com.example.davidcocom.proyectomoviles.db.UserDataSource;
import com.example.davidcocom.proyectomoviles.models.Code;
import com.example.davidcocom.proyectomoviles.models.User;

import java.util.ArrayList;
import java.util.List;


public class LoginActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

      createDatabase();

      SharedPreferences sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE);
      boolean login = sharedPreferences.getBoolean("login", false);
      boolean exit = sharedPreferences.getBoolean("exit", false);

      if (exit) {
        SharedPreferences.Editor editor = getSharedPreferences("session", Context.MODE_PRIVATE).edit();
        editor.putBoolean("exit", false);
        editor.apply();
        finish();
      } else if (login) {
        Intent intent = new Intent(getApplicationContext(), RestaurantList.class);
        startActivity(intent);
      }

    }

  @Override
  public void onResume() {
    super.onResume();
    SharedPreferences sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE);
    boolean login = sharedPreferences.getBoolean("login", false);
    boolean exit = sharedPreferences.getBoolean("exit", false);

    if (exit) {
      SharedPreferences.Editor editor = getSharedPreferences("session", Context.MODE_PRIVATE).edit();
      editor.putBoolean("exit", false);
      editor.apply();
      finish();
    } else if (login) {
      Intent intent = new Intent(getApplicationContext(), RestaurantList.class);
      startActivity(intent);
    }
  }

    private void createDatabase(){
      UserDataSource userDS = new UserDataSource(getApplicationContext());
      userDS.open();
      if (userDS.getAllUsers().isEmpty()) {
        userDS.insertUser(new User("alex", "1234"));
          userDS.insertUser(new User("carla", "4321"));
      }

      userDS.close();

        CodesDataSource codesDS = new CodesDataSource(getApplicationContext());
        codesDS.open();
        if (codesDS.getAllCodes().isEmpty()) {
            codesDS.insertCode(new Code("alex", 5));
            codesDS.insertCode(new Code("carla", 1));
            codesDS.insertCode(new Code("alice", 3));
        }
        codesDS.close();
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LoginFragment(), "Iniciar sesión");
        adapter.addFragment(new RegistrationFragment(), "Registrate");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}