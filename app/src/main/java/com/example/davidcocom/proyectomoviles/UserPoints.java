package com.example.davidcocom.proyectomoviles;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.davidcocom.proyectomoviles.db.PointsDataSource;
import com.example.davidcocom.proyectomoviles.models.Points;

import java.util.ArrayList;

public class UserPoints extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_points);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Restaurants Points");
        getDataBase();
    }

    private void getDataBase(){
        PointsDataSource pointsDS = new PointsDataSource(getApplicationContext());
        pointsDS.open();
        SharedPreferences sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE);
        String username = sharedPreferences.getString("username", null);
        ArrayList<Points> pointsArray = (ArrayList<Points>) pointsDS.getAllPoints(username);
        ListView list = (ListView)findViewById(R.id.list_points);
        TextView user = (TextView) findViewById(R.id.username_points);
        user.setText(username);

        if (!pointsArray.isEmpty()) {
            ArrayAdapter<Points> adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, pointsArray);
            if (list != null) {
                list.setAdapter(adapter);
            }else {
                Toast.makeText(this, "List empty", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "You have no points", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        SharedPreferences.Editor sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE).edit();
        sharedPreferences.putBoolean("restaurants", true);
        sharedPreferences.apply();
        finish();
    }
}
