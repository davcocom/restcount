package com.example.davidcocom.proyectomoviles.db.contract;

import android.provider.BaseColumns;

public class Contract implements BaseColumns {
  public static final String USERS_TABLE = "Users";
  public static final String POINTS_TABLE = "Points";
  public static final String CODES_TABLE = "Codes";

  public static final String USERNAME_COLUMN = "username";
  public static final String PASSWORD_COLUMN = "password";
  public static final String RESTAURANT_COLUMN = "restaurant";
  public static final String SCORE_COLUMN = "score";
  public static final String CODE_COLUMN = "code";

  private static final String TEXT_TYPE = " TEXT";
  private static final String INTEGER_TYPE = " INTEGER";
  private static final String COMMA_SEP = ",";

  public static final String SQL_CREATE_USERS =
      "CREATE TABLE " + USERS_TABLE + " (" + USERNAME_COLUMN + " TEXT PRIMARY KEY," +
          PASSWORD_COLUMN + TEXT_TYPE +
          " )";

  public static final String SQL_DELETE_USERS =
      "DROP TABLE IF EXISTS " + USERS_TABLE;

  public static final String SQL_CREATE_POINTS =
      "CREATE TABLE " + POINTS_TABLE + " (" + USERNAME_COLUMN + TEXT_TYPE + COMMA_SEP +
          RESTAURANT_COLUMN + TEXT_TYPE + COMMA_SEP +
          SCORE_COLUMN + INTEGER_TYPE + COMMA_SEP +
          "PRIMARY KEY (" + USERNAME_COLUMN + COMMA_SEP + RESTAURANT_COLUMN + ")" +
          " )";

  public static final String SQL_DELETE_POINTS =
      "DROP TABLE IF EXISTS " + POINTS_TABLE;

  public static final String SQL_CREATE_CODES =
      "CREATE TABLE " + CODES_TABLE + " (" + CODE_COLUMN + " TEXT PRIMARY KEY," +
          SCORE_COLUMN + INTEGER_TYPE +
          " )";

  public static final String SQL_DELETE_CODES =
      "DROP TABLE IF EXISTS " + CODES_TABLE;
}
