package com.example.davidcocom.proyectomoviles;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.card.OnActionClickListener;
import com.dexafree.materialList.card.action.TextViewAction;
import com.dexafree.materialList.view.MaterialListView;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.rey.material.widget.Spinner;
import com.squareup.picasso.RequestCreator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class RestaurantList extends AppCompatActivity implements SensorEventListener {

    private Spinner spinner;
    private MaterialListView mListView;
    private String url = null;
    private FoursquareAPI foursquareAPI = new FoursquareAPI();

    private String longitude;
    private String latitude;

    private Handler handler;
    private ProgressDialog progress;
    private Context context;

    private boolean beaconOn = false;
    private BeaconManager beaconManager;

    private SensorManager sensorManager;
    private Sensor accelerometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_list);
        setGPSConfig();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListView.smoothScrollToPosition(0);
            }
        });

        setupSpinner();

        showProgressBarWhileGettingLocation();

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        SharedPreferences.Editor sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE).edit();
        sharedPreferences.putBoolean("restaurants", false);
        sharedPreferences.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        SharedPreferences.Editor sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE).edit();
        sharedPreferences.putBoolean("restaurants", false);
        sharedPreferences.apply();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    public void logout(View view) {
        SharedPreferences.Editor sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE).edit();
        sharedPreferences.putBoolean("login", false);
        sharedPreferences.putBoolean("exit", false);
        sharedPreferences.putString("username", null);
        sharedPreferences.apply();

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void viewPoints(View view){
        Intent intent = new Intent(getApplicationContext(), UserPoints.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        SharedPreferences.Editor sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE).edit();
        sharedPreferences.putBoolean("exit", true);
        sharedPreferences.apply();
        finish();
    }

    private void showProgressBarWhileGettingLocation() {
        context = RestaurantList.this;
        progress = new ProgressDialog(this);
        progress.setTitle("Espere...");
        progress.setMessage("Estamos obteniendo tu ubicación");
        progress.setCancelable(false);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        handler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                progress.dismiss();
                //cuando está listo se llenan las cartitas
                setupCards();
                super.handleMessage(msg);
            }

        };
        progress.show();
        new Thread() {
            public void run() {
                //espera a que se tenga una ubicación
                while (longitude == null) {

                }
                // Write Your Downloading logic here
                // at the end write this.
                handler.sendEmptyMessage(0);
            }

        }.start();

    }

    private void analyzeJson() {
        try {
            JSONArray places = new JSONTask().execute(this.url).get().
                    getJSONObject("response").getJSONArray("venues");
            for (int i = 0; i < places.length(); i++) {
                JSONObject place = places.getJSONObject(i);
                String name = place.getString("name");
                String id = place.getString("id");
                JSONArray jAddress = place.getJSONObject("location").
                        getJSONArray("formattedAddress");
                String address = "";
                for (int j = 0; j < jAddress.length(); j++) {
                    address += jAddress.getString(j) + "\n";
                }
                String distance = "A " + String.valueOf(place.getJSONObject("location").
                        get("distance")) + " metros.";
                JSONObject icon = place.getJSONArray("categories").
                        getJSONObject(0).getJSONObject("icon");
                String iconUrl = icon.getString("prefix") + "120.png";

                Log.d("JsonIcon", iconUrl);
                Log.d("JsonDistance", distance);

                addNewCard(name, distance, address, iconUrl, id);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void setupCards() {
        mListView = (MaterialListView) findViewById(R.id.material_listView);
        mListView.getAdapter().clearAll();
        url = getFoursquareURL();
        Log.d("url", url);

        analyzeJson();
        mListView.smoothScrollToPosition(0);
    }

    private String getFoursquareURL() {
        String today = "20160518";
        return "https://api.foursquare.com/v2/venues/search?" +
                "ll=" +
                latitude +
                "," +
                longitude +
                "&sortByDistance=1" +
                "&client_id=" + FoursquareAPI.clientID +
                "&client_secret=" + FoursquareAPI.clientSecret +
                "&v=" + today +
                "&radius=3000" +
                "&categoryId=" +
                foursquareAPI.categories.get(spinner.getSelectedItemPosition());
    }

    private void addNewCard(final String title, String subtitle, String content, String imageURL, final String id) {
        URL url;
        Card card = null;
        String bg = "#FF5722";
        try {
            url = new URL(imageURL);
            card = new Card.Builder(this)
                    .withProvider(new CardProvider())
                    .setLayout(R.layout.material_basic_image_buttons_card_layout)
                    .setBackgroundColor(Color.parseColor(bg))
                    .setTitle(title)
                    .setTitleColor(Color.WHITE)
                    .setSubtitle(subtitle)
                    .setSubtitleColor(Color.WHITE)
                    .setDescription(content)
                    .setDescriptionColor(Color.WHITE)
                    .setDrawable(new DrawableImageTask().execute(url).get())
                    .setDrawableConfiguration(new CardProvider.OnImageConfigListener() {
                        @Override
                        public void onImageConfigure(@NonNull RequestCreator requestCreator) {
                            requestCreator.fit();
                        }
                    })
                    .addAction(R.id.right_text_button, new TextViewAction(this)
                            .setText("Exchange")
                            .setTextResourceColor(R.color.orange_button)
                            .setListener(new OnActionClickListener() {
                                @Override
                                public void onActionClicked(View view, Card card) {
                                    turnBeaconOn(title);
                                }
                            }))
                    .endConfig()
                    .build();
        } catch (InterruptedException | ExecutionException | MalformedURLException e) {
            e.printStackTrace();
        }

        mListView.getAdapter().add(card);
    }

    private void setupSpinner() {
        ArrayList<String> regions = new ArrayList<>();
        regions.add("Restaurantes");
        regions.add("Comida Rápida");
        regions.add("Mexicana");
        regions.add("Comida Italiana");
        regions.add("Sushi");
        regions.add("Hamburguesas");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, regions);

        spinner = (Spinner) findViewById(R.id.spinner_categories);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner parent, View view, int position, long id) {
                setupCards();
            }

        });
    }

    private void setGPSConfig() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
    }

    private void turnBeaconOn(final String restaurant) {
        beaconManager = new BeaconManager(getApplicationContext());

        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onEnteredRegion(Region region, List<Beacon> list) {
                showNotification(
                    "Bienvenido a " + restaurant,
                    "Pregunta en la caja por el código para acumular puntos.");
                Intent intent = new Intent(getApplicationContext(), PromptCode.class);
                intent.putExtra("restaurant", restaurant);
                startActivity(intent);
            }
            @Override
            public void onExitedRegion(Region region) {
                showNotification(
                    "Gracias por su visita",
                    "Esperamos haya tenido una experiencia placentera."
                );
            }
        });

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconOn = true;
                beaconManager.startMonitoring(new Region(
                    "monitored region",
                    UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
                    63463, 21120));
            }
        });
    }

    public void showNotification(String title, String message) {
        Intent notifyIntent = new Intent(this, PromptCode.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
            new Intent[] { notifyIntent }, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
            .setSmallIcon(android.R.drawable.ic_dialog_info)
            .setContentTitle(title)
            .setContentText(message)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), "Active su ubicación...", Toast.LENGTH_SHORT).show();
            finish();
        }
    };

    @Override
    public void onSensorChanged(SensorEvent event) {
        float[] values = event.values;
        float x = values[0];
        float y = values[1];
        float z = values[2];

        if (x >= 11.0 || y >= 11.0 || z >= 11.0 || x <= -11.0 || y <= -11.0 || z <= -11.0) {
            Intent intent = new Intent(getApplicationContext(), UserPoints.class);
            startActivity(intent);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
}
