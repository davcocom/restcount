package com.example.davidcocom.proyectomoviles.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.davidcocom.proyectomoviles.db.contract.Contract;
import com.example.davidcocom.proyectomoviles.db.helper.Helper;
import com.example.davidcocom.proyectomoviles.models.Code;
import com.example.davidcocom.proyectomoviles.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: alex
 * Date created: 5/26/16
 * Project: restcount
 */
public class CodesDataSource {
  private SQLiteDatabase database;
  private Helper helper;
  private String[] allColumns = {
      Contract.CODE_COLUMN,
      Contract.SCORE_COLUMN
  };

  public CodesDataSource(Context context) {
    helper = new Helper(context);
  }

  public void open() throws SQLException {
    database = helper.getWritableDatabase();
  }

  public void close() {
    helper.close();
  }

  public long insertCode(Code stdToInsert) {
    ContentValues values = new ContentValues();
    values.put(Contract.CODE_COLUMN, stdToInsert.getCode());
    values.put(Contract.SCORE_COLUMN, stdToInsert.getScore());
    long newRowId;
    newRowId = database.insert(Contract.CODES_TABLE, null, values);
    return newRowId;
  }

  public long updateCode(Code code) {
    ContentValues values = new ContentValues();
    values.put(Contract.SCORE_COLUMN, code.getScore());
    long newRowId;
    newRowId = database.update(Contract.USERS_TABLE, values,
        Contract.CODE_COLUMN + "=?", new String[]{code.getCode()});

    return newRowId;
  }

  public long deleteCode(Code stdToDelete) {
    String whereClause = Contract.CODE_COLUMN + "=?";
    String[] whereArgs = new String[]{stdToDelete.getCode()};
    return database.delete(Contract.CODES_TABLE, whereClause, whereArgs);
  }

  public List<Code> getAllCodes() {
    List<Code> codes = new ArrayList<>();
    Code code;
    Cursor cursor = database.query(Contract.CODES_TABLE, allColumns, null, null, null, null, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      code = cursorToCode(cursor);
      codes.add(code);
      cursor.moveToNext();
    }
    cursor.close();
    return codes;
  }

  private Code cursorToCode(Cursor cursor) {
    return new Code(cursor.getString(0), cursor.getInt(1));
  }
}
