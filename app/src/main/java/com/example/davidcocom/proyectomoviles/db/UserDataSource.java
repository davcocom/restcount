package com.example.davidcocom.proyectomoviles.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.davidcocom.proyectomoviles.db.contract.Contract;
import com.example.davidcocom.proyectomoviles.db.helper.Helper;
import com.example.davidcocom.proyectomoviles.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: alex
 * Date created: 5/26/16
 * Project: restcount
 */
public class UserDataSource {
  private SQLiteDatabase database;
  private Helper helper;
  private String[] allColumns = {
      Contract.USERNAME_COLUMN,
      Contract.PASSWORD_COLUMN
  };

  public UserDataSource(Context context) {
    helper = new Helper(context);
  }

  public void open() throws SQLException {
    database = helper.getWritableDatabase();
  }

  public void close() {
    helper.close();
  }

  public long insertUser(User stdToInsert) {
    ContentValues values = new ContentValues();
    values.put(Contract.USERNAME_COLUMN, stdToInsert.getUsername());
    values.put(Contract.PASSWORD_COLUMN, stdToInsert.getPassword());
    long newRowId;
    newRowId = database.insert(Contract.USERS_TABLE, null, values);
    return newRowId;
  }

  public long updateUser(User user) {
    ContentValues values = new ContentValues();
    values.put(Contract.PASSWORD_COLUMN, user.getPassword());
    long newRowId;
    newRowId = database.update(Contract.USERS_TABLE, values,
        Contract.USERNAME_COLUMN + "=?", new String[]{user.getUsername()});

    return newRowId;
  }

  public long deleteUser(User stdToDelete) {
    String whereClause = Contract.USERNAME_COLUMN + "=?";
    String[] whereArgs = new String[]{stdToDelete.getUsername()};
    return database.delete(Contract.USERS_TABLE, whereClause, whereArgs);
  }

  public List<User> getAllUsers() {
    List<User> users = new ArrayList<>();
    User user;
    Cursor cursor = database.query(Contract.USERS_TABLE, allColumns, null, null, null, null, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      user = cursorToUser(cursor);
      users.add(user);
      cursor.moveToNext();
    }
    cursor.close();
    return users;
  }

  private User cursorToUser(Cursor cursor) {
    return new User(cursor.getString(0), cursor.getString(1));
  }
}
