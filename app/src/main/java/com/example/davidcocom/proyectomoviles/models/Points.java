package com.example.davidcocom.proyectomoviles.models;

import java.io.Serializable;

public class Points implements Serializable {
  private String username;
  private String restaurantId;
  private int score;

  public Points() {
  }

  public Points(String username, String restaurantId, int score) {
    this.username = username;
    this.restaurantId = restaurantId;
    this.score = score;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getRestaurantId() {
    return restaurantId;
  }

  public void setRestaurantId(String restaurantId) {
    this.restaurantId = restaurantId;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Points points = (Points) o;

    if (!username.equals(points.username)) return false;
    return restaurantId.equals(points.restaurantId);
  }

  @Override
  public int hashCode() {
    int result = username.hashCode();
    result = 31 * result + restaurantId.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return restaurantId + ": " + score + " points";
  }
}
