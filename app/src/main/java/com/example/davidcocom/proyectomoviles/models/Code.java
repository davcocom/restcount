package com.example.davidcocom.proyectomoviles.models;

import java.io.Serializable;

public class Code implements Serializable {
  private String code;
  private int score;

  public Code() {
  }

  public Code(String code, int score) {
    this.code = code;
    this.score = score;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public int getScore() {
    return score;
  }

  public void setScore(int score) {
    this.score = score;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Code code1 = (Code) o;

    return code.equals(code1.code);

  }

  @Override
  public int hashCode() {
    return code.hashCode();
  }
}
