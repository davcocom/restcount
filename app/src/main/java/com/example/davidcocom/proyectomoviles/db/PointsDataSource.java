package com.example.davidcocom.proyectomoviles.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.davidcocom.proyectomoviles.db.contract.Contract;
import com.example.davidcocom.proyectomoviles.db.helper.Helper;
import com.example.davidcocom.proyectomoviles.models.Points;
import com.example.davidcocom.proyectomoviles.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: alex
 * Date created: 5/26/16
 * Project: restcount
 */
public class PointsDataSource {
  private SQLiteDatabase database;
  private Helper helper;
  private String[] allColumns = {
      Contract.USERNAME_COLUMN,
      Contract.RESTAURANT_COLUMN,
      Contract.SCORE_COLUMN
  };

  public PointsDataSource(Context context) {
    helper = new Helper(context);
  }

  public void open() throws SQLException {
    database = helper.getWritableDatabase();
  }

  public void close() {
    helper.close();
  }

  public long insertPoints(Points stdToInsert) {
    ContentValues values = new ContentValues();
    values.put(Contract.USERNAME_COLUMN, stdToInsert.getUsername());
    values.put(Contract.RESTAURANT_COLUMN, stdToInsert.getRestaurantId());
    values.put(Contract.SCORE_COLUMN, stdToInsert.getScore());
    long newRowId;
    newRowId = database.insert(Contract.POINTS_TABLE, null, values);
    return newRowId;
  }

  public long updatePoints(Points points) {
    ContentValues values = new ContentValues();
    values.put(Contract.SCORE_COLUMN, points.getScore());
    long newRowId;
    newRowId = database.update(Contract.POINTS_TABLE, values,
        Contract.USERNAME_COLUMN + "=? and " + Contract.RESTAURANT_COLUMN + "=?", new String[]{points.getUsername(), points.getRestaurantId()});

    return newRowId;
  }

  public long deleteUser(Points stdToDelete) {
    String whereClause = Contract.USERNAME_COLUMN + "=? and " + Contract.RESTAURANT_COLUMN + "=?";
    String[] whereArgs = new String[]{stdToDelete.getUsername(), stdToDelete.getRestaurantId()};
    return database.delete(Contract.POINTS_TABLE, whereClause, whereArgs);
  }

  public List<Points> getAllPoints() {
    List<Points> points = new ArrayList<>();
    Points point;
    Cursor cursor = database.query(Contract.POINTS_TABLE, allColumns, null, null, null, null, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      point = cursorToPoints(cursor);
      points.add(point);
      cursor.moveToNext();
    }
    cursor.close();
    return points;
  }

  public List<Points> getAllPoints(String username) {
    List<Points> points = new ArrayList<>();
    Points point;
    String whereClause = Contract.USERNAME_COLUMN + "=?";
    String[] whereArgs = new String[] {username};
    Cursor cursor = database.query(Contract.POINTS_TABLE, allColumns, whereClause, whereArgs, null, null, null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()) {
      point = cursorToPoints(cursor);
      points.add(point);
      cursor.moveToNext();
    }
    cursor.close();
    return points;
  }

  private Points cursorToPoints(Cursor cursor) {
    return new Points(cursor.getString(0), cursor.getString(1), cursor.getInt(2));
  }
}
