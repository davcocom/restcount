package com.example.davidcocom.proyectomoviles;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.davidcocom.proyectomoviles.db.CodesDataSource;
import com.example.davidcocom.proyectomoviles.db.PointsDataSource;
import com.example.davidcocom.proyectomoviles.models.Code;
import com.example.davidcocom.proyectomoviles.models.Points;

import java.util.ArrayList;

public class PromptCode extends AppCompatActivity {

    private String restaurant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prompt_code);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Enter Code");
        restaurant = getIntent().getStringExtra("restaurant");

        SharedPreferences sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE);
        boolean restaurants = sharedPreferences.getBoolean("restaurants", false);

        if (restaurants) {
            SharedPreferences.Editor editor = getSharedPreferences("session", Context.MODE_PRIVATE).edit();
            editor.putBoolean("restaurants", false);
            editor.apply();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE);
        boolean restaurants = sharedPreferences.getBoolean("restaurants", false);

        if (restaurants) {
            SharedPreferences.Editor editor = getSharedPreferences("session", Context.MODE_PRIVATE).edit();
            editor.putBoolean("restaurants", false);
            editor.apply();
            finish();
        }
    }

    public void addScore(View view) {
        EditText editTextCode = (EditText) findViewById(R.id.txt_code);
        String str_code = editTextCode.getText().toString();

        CodesDataSource codesDS = new CodesDataSource(getApplicationContext());
        codesDS.open();
        ArrayList<Code> codes = (ArrayList<Code>) codesDS.getAllCodes();
        codesDS.close();

        Code code = new Code(str_code, 0);
        int indexOfCode = codes.indexOf(code);
        if (indexOfCode != -1) {
            editTextCode.setText("");
            code = codes.get(indexOfCode);

            SharedPreferences sharedPreferences = getSharedPreferences("session", Context.MODE_PRIVATE);
            String username = sharedPreferences.getString("username", null);

            PointsDataSource pointsDS = new PointsDataSource(getApplicationContext());
            pointsDS.open();
            ArrayList<Points> pointsArray = (ArrayList<Points>) pointsDS.getAllPoints(username);

            Points points = new Points(username, restaurant, code.getScore());

            int indexOfPoints = pointsArray.indexOf(points);
            if (indexOfPoints != -1) {
                Points points1 = pointsArray.get(indexOfPoints);
                points1.setScore(points1.getScore() + code.getScore());
                pointsDS.updatePoints(points1);
                Toast.makeText(this, "Tienes " + points1.getScore() + " puntos en " + restaurant, Toast.LENGTH_SHORT).show();
            } else {
                pointsDS.insertPoints(points);
                Toast.makeText(this, "Se crearon " + points.getScore() + " puntos en " + restaurant, Toast.LENGTH_SHORT).show();
            }

            Intent intent = new Intent(this, UserPoints.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Código inválido", Toast.LENGTH_SHORT).show();
        }
    }
}
