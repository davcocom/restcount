package com.example.davidcocom.proyectomoviles;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.davidcocom.proyectomoviles.db.UserDataSource;
import com.example.davidcocom.proyectomoviles.models.User;

public class RegistrationFragment extends Fragment {

    public RegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registration, container, false);

        Button btnLogin = (Button) view.findViewById(R.id.btn_register);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        return view;
    }

    private void register() {
        EditText editTextUsername = (EditText) getActivity().findViewById(R.id.username_registration);
        String username = editTextUsername.getText().toString();
        EditText editTextPassword = (EditText) getActivity().findViewById(R.id.password_registration);
        String password = editTextPassword.getText().toString();

        if (!username.equals("") && !password.equals("")) {
            UserDataSource userDS = new UserDataSource(getContext());
            userDS.open();
            User user = new User(username, password);
            long rowId = userDS.insertUser(user);
            if (rowId != -1) {
                editTextUsername.setText("");
                editTextPassword.setText("");

                SharedPreferences.Editor sharedPreferences = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE).edit();
                sharedPreferences.putBoolean("login", true);
                sharedPreferences.putString("username", username);
                sharedPreferences.apply();

                Intent intent = new Intent(getActivity(), RestaurantList.class);
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), "Usuario ya registrado", Toast.LENGTH_SHORT).show();
            }
            userDS.close();
        } else {
            Toast.makeText(getActivity(), "No pueden estar vacíos los campos", Toast.LENGTH_SHORT).show();
        }
    }

}
