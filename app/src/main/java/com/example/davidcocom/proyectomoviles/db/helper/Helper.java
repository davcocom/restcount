package com.example.davidcocom.proyectomoviles.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.davidcocom.proyectomoviles.db.contract.Contract;

public class Helper extends SQLiteOpenHelper {
  public static final int DATABASE_VERSION = 1;
  public static final String DATABASE_NAME = "RestCount.db";

  public Helper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(Contract.SQL_CREATE_USERS);
    db.execSQL(Contract.SQL_CREATE_POINTS);
    db.execSQL(Contract.SQL_CREATE_CODES);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL(Contract.SQL_DELETE_CODES);
    db.execSQL(Contract.SQL_DELETE_POINTS);
    db.execSQL(Contract.SQL_DELETE_USERS);
    onCreate(db);
  }

  @Override
  public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    onUpgrade(db, oldVersion, newVersion);
  }
}
