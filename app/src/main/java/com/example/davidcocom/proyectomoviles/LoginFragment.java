package com.example.davidcocom.proyectomoviles;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.davidcocom.proyectomoviles.db.UserDataSource;
import com.example.davidcocom.proyectomoviles.models.User;

import java.util.ArrayList;


public class LoginFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

  @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        Button btnLogin = (Button) view.findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        return view;
    }

  private void login() {
    EditText editTextUsername = (EditText) getActivity().findViewById(R.id.username_login);
    String username = editTextUsername.getText().toString();
    EditText editTextPassword = (EditText) getActivity().findViewById(R.id.password_login);
    String password = editTextPassword.getText().toString();

    UserDataSource userDS = new UserDataSource(getContext());
    userDS.open();
    ArrayList<User> users = (ArrayList<User>) userDS.getAllUsers();
    userDS.close();

    User user = new User(username, password);
    if (users.contains(user)) {
      editTextUsername.setText("");
      editTextPassword.setText("");

      SharedPreferences.Editor sharedPreferences = getActivity().getSharedPreferences("session", Context.MODE_PRIVATE).edit();
      sharedPreferences.putBoolean("login", true);
      sharedPreferences.putString("username", username);
      sharedPreferences.apply();

      Intent intent = new Intent(getActivity(), RestaurantList.class);
      startActivity(intent);
    } else {
      Toast.makeText(getActivity(), "No se pudo iniciar sesión", Toast.LENGTH_SHORT).show();
    }
  }
}
